import sys
sys.path.append('./')

from random import randint
from timezone import TimeZone


tz = TimeZone()

lista_locais = tz.locais()

zona = lista_locais[randint(0,len(lista_locais))]

print(tz.hora_atual(zona))