import requests
import json

class TimeZone:  
    def hora_atual(self, zona):
        print(zona)
        return json.loads(requests.get("http://worldtimeapi.org/api/timezone/"+zona).text)

    def locais(self):
        return json.loads(requests.get("http://worldtimeapi.org/api/timezone").text)